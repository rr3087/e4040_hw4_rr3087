"""
Source Code for Homework 4.b of ECBM E4040, Fall 2016, Columbia University

"""

import os
import timeit
import inspect
import sys
import numpy
from collections import OrderedDict

import theano
import theano.tensor as T
from theano.tensor.nnet import conv2d
from theano.tensor.signal import downsample

from hw4_utils import contextwin, shared_dataset, load_data, shuffle, conlleval, check_dir
from hw4_nn import myMLP, train_nn

def gen_parity_pair(nbit, num):
    """
    Generate binary sequences and their parity bits

    :type nbit: int
    :param nbit: length of binary sequence

    :type num: int
    :param num: number of sequences

    """
    X = numpy.random.randint(2, size=(num, nbit))
    Y = numpy.mod(numpy.sum(X, axis=1), 2)
    
    return X, Y

def gen_parity_rnn(x):
    y = numpy.zeros(x.shape)
    for i in range(x.shape[1]):
        if(i==0):
            y[:,i] = numpy.mod(x[:,i],2)
        else:
            y[:,i] = numpy.mod(numpy.sum(x[:,0:(i+1)], axis=1),2) 
    return y.astype(int)


#TODO: implement RNN class to learn parity function
# class RNN(object):
#     """ Elman Neural Net Model Class
#     """
#     def __init__(self, input, n_in, n_hidden, n_out, activation=T.tanh):
#         """Initialize the parameters for the RNNSLU

#         :type nh: int
#         :param nh: dimension of the hidden layer

#         :type nc: int
#         :param nc: number of classes

#         :type ne: int
#         :param ne: number of word embeddings in the vocabulary

#         :type de: int
#         :param de: dimension of the word embeddings

#         :type cs: int
#         :param cs: word window context size

#         :type normal: boolean
#         :param normal: normalize word embeddings after each update or not.

#         """
#         # parameters of the model
#         self.input = input
#         self.activation = activation
#         self.softmax = T.nnet.softmax
#         self.wx = theano.shared(name='wx',
#                                 value = numpy.asarray(numpy.random.uniform(size=(n_in, n_hidden),
#                                                  low=-.01, high=.01),
#                                                  dtype=theano.config.floatX))
#         self.wh = theano.shared(name='wh',
#                                 value=numpy.asarray(numpy.random.uniform(size=(n_hidden, n_hidden),
#                                                                    low=-.01, high=.01),
#                                                  dtype=theano.config.floatX))
#         self.w = theano.shared(name='w',
#                                value = numpy.asarray(numpy.random.uniform(size=(n_hidden, n_out),
#                                                   low=-.01, high=.01),
#                                                   dtype=theano.config.floatX))
#         self.bh = theano.shared(name='bh',
#                                 value=numpy.zeros((n_hidden,), dtype=theano.config.floatX))
#         self.b = theano.shared(name='b',
#                                value=numpy.zeros((n_out,), dtype=theano.config.floatX))
#         self.h0 = theano.shared(name='h0',value=numpy.zeros((n_hidden,), dtype=theano.config.floatX))

#         # bundle
#         self.params = [self.wx, self.wh, self.w,
#                        self.bh, self.b, self.h0]
#         def recurrence(x_t, h_tm1):
#             h_t = T.nnet.sigmoid(T.dot(x_t, self.wx)
#                                  + T.dot(h_tm1, self.wh) + self.bh)
#             s_t = (T.dot(h_t, self.w) + self.b)
#             return [h_t, s_t]
        
#         [self.h0, self.y_pred], _ = theano.scan(fn=recurrence,
#                                 sequences=self.input,
#                                 outputs_info=[self.h0, None])
        
#         self.L1 = abs(self.w.sum()) + abs(self.wx.sum()) + abs(self.wh.sum())
                                  
#         self.L2_sqr = (self.w ** 2).sum() + (self.wx ** 2).sum() + (self.wh ** 2).sum()


        
       
#         self.p_y_given_x = T.nnet.softmax(self.y_pred)
#         self.y_out = T.argmax(self.p_y_given_x, axis=-1)
#         self.loss = lambda y: -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])
        



#     def errors(self, y):
#         if y.ndim != self.y_out.ndim:
#             raise TypeError('y should have the same shape as self.y_out',
#                 ('y', y.type, 'y_out', self.y_out.type))
#         if y.dtype.startswith('int'):
#             return T.mean(T.neq(self.y_out, y))

#     def negative_log_likelihood(self, y):
#         return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])


class RNN(object):
    #def __init__(self, nh, nc, activation_fnc=T.tanh):
    def __init__(self, nh, nc):
        """Initialize the parameters for the RNNSLU

        :type nh: int
        :param nh: dimension of the hidden layer

        :type nc: int
        :param nc: number of classes

        :type ne: int
        :param ne: number of word embeddings in the vocabulary

        :type de: int
        :param de: dimension of the word embeddings

        :type cs: int
        :param cs: word window context size

        :type normal: boolean
        :param normal: normalize word embeddings after each update or not.

        """
        # parameters of the model
        self.wx = theano.shared(name='wx',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (1,nh))
                                .astype(theano.config.floatX))
        self.wh = theano.shared(name='wh',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))
        self.w = theano.shared(name='w',
                               value=0.2 * numpy.random.uniform(-1.0, 1.0,
                               (nh, nc))
                               .astype(theano.config.floatX))
        self.bh = theano.shared(name='bh',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        self.b = theano.shared(name='b',
                               value=numpy.zeros(nc,
                               dtype=theano.config.floatX))
        self.h0 = theano.shared(name='h0',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        
        # bundle
        self.params = [self.wx, self.wh, self.w,
                       self.bh, self.b, self.h0]
        
        x = T.matrix('x')
        y = T.ivector('y')
        
        def recurrence(x_t, h_tm1):
            h_t = T.nnet.sigmoid(T.dot(x_t, self.wx)
                                 + T.dot(h_tm1, self.wh) + self.bh)
            s_t = T.nnet.softmax(T.dot(h_t, self.w) + self.b)
            return [h_t, s_t]
        
        [h, s], _ = theano.scan(fn=recurrence,
                                sequences=x,
                                outputs_info=[self.h0, None],n_steps=x.shape[0])
        
        #self.L1 = abs(self.w.sum()) + abs(self.wx.sum()) + abs(self.wh.sum())
                                  
        #self.L2_sqr = (self.w ** 2).sum() + (self.wx ** 2).sum() + (self.wh ** 2).sum()
        
        # n_bits,1,nc
        #p_y_given_x = s[-1, 0, :]
        p_y_given_x = s[:, 0, :]
        
        y_pred = T.argmax(p_y_given_x, axis=1)
        
        err = T.mean(T.neq(y_pred, y))
        
        # cost and gradients and learning rate
        lr = T.scalar('lr')

        # continue from here
        parity_nll = -T.mean(T.log(p_y_given_x)
                               [T.arange(x.shape[0]), y])
        
        parity_gradients = T.grad(parity_nll, self.params)
        
        parity_updates = OrderedDict((p, p - lr*g)
                                       for p, g in
                                       zip(self.params, parity_gradients))

        # theano functions to compile
        self.classify_compute_err = theano.function(inputs=[x, y], outputs=err, allow_input_downcast=True)
        self.parity_train = theano.function(inputs=[x, y, lr],
                                              outputs=parity_nll,
                                              updates=parity_updates,
                                              allow_input_downcast = True
                                              )



class RNN2(object):
    """ Elman Neural Net Model Class
    """
    def __init__(self, input, n_in, n_hidden1, n_hidden2, n_out, activation=T.tanh):
        """Initialize the parameters for the RNNSLU

        :type nh: int
        :param nh: dimension of the hidden layer

        :type nc: int
        :param nc: number of classes

        :type ne: int
        :param ne: number of word embeddings in the vocabulary

        :type de: int
        :param de: dimension of the word embeddings

        :type cs: int
        :param cs: word window context size

        :type normal: boolean
        :param normal: normalize word embeddings after each update or not.

        """
        # parameters of the model
        self.input = input
        self.activation = activation
        self.softmax = T.nnet.softmax
        self.wx = theano.shared(name='wx',
                                value = numpy.asarray(numpy.random.uniform(size=(n_in, n_hidden1),
                                                 low=-.01, high=.01),
                                                 dtype=theano.config.floatX))
        self.wx2 = theano.shared(name='wx2',
                                value = numpy.asarray(numpy.random.uniform(size=(n_hidden1, n_hidden2),
                                                 low=-.01, high=.01),
                                                 dtype=theano.config.floatX)) 

        self.wh = theano.shared(name='wh',
                                value=numpy.asarray(numpy.random.uniform(size=(n_hidden, n_hidden),
                                                                   low=-.01, high=.01),
                                                 dtype=theano.config.floatX))
        self.w = theano.shared(name='w',
                               value = numpy.asarray(numpy.random.uniform(size=(n_hidden, n_out),
                                                  low=-.01, high=.01),
                                                  dtype=theano.config.floatX))
        self.bh = theano.shared(name='bh',
                                value=numpy.zeros((n_hidden,), dtype=theano.config.floatX))
        self.b = theano.shared(name='b',
                               value=numpy.zeros((n_out,), dtype=theano.config.floatX))
        self.h0 = theano.shared(name='h0',value=numpy.zeros((n_hidden,), dtype=theano.config.floatX))

        # bundle
        self.params = [self.wx, self.wh, self.w,
                       self.bh, self.b, self.h0]
        def recurrence(x_t, h_tm1):
            h_t = T.nnet.sigmoid(T.dot(x_t, self.wx)
                                 + T.dot(h_tm1, self.wh) + self.bh)
            s_t = (T.dot(h_t, self.w) + self.b)
            return [h_t, s_t]
        
        [self.h0, self.y_pred], _ = theano.scan(fn=recurrence,
                                sequences=self.input,
                                outputs_info=[self.h0, None])
        
        self.L1 = abs(self.w.sum()) + abs(self.wx.sum()) + abs(self.wh.sum())
                                  
        self.L2_sqr = (self.w ** 2).sum() + (self.wx ** 2).sum() + (self.wh ** 2).sum()


        
       
        self.p_y_given_x = T.nnet.softmax(self.y_pred)
        self.y_out = T.argmax(self.p_y_given_x, axis=-1)
        self.loss = lambda y: -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])
        



    def errors(self, y):
        if y.ndim != self.y_out.ndim:
            raise TypeError('y should have the same shape as self.y_out',
                ('y', y.type, 'y_out', self.y_out.type))
        if y.dtype.startswith('int'):
            return T.mean(T.neq(self.y_out, y))

    def negative_log_likelihood(self, y):
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])



#TODO: implement LSTM class to learn parity function
class LSTM(object):
    def __init__(self, nh, nc):

        # input gate parameters
        self.wi = theano.shared(name='wi',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (1,nh))
                                .astype(theano.config.floatX))
        
        self.ui = theano.shared(name='ui',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))
        
        # parameters to compute candidate state value
        self.wc = theano.shared(name='wc',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (1,nh))
                                .astype(theano.config.floatX))
        
        self.uc = theano.shared(name='uc',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))
        
        # forget gate parameters
        self.wf = theano.shared(name='wf',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (1,nh))
                                .astype(theano.config.floatX))
        
        self.uf = theano.shared(name='uf',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))

        # output gate parameters
        self.wo = theano.shared(name='wo',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (1,nh))
                                .astype(theano.config.floatX))
        
        self.uo = theano.shared(name='uo',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))
        
        self.vo = theano.shared(name='vo',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh, nh))
                                .astype(theano.config.floatX))
        
        # final output parameters
        self.wy = theano.shared(name='wy',
                                value=0.2 * numpy.random.uniform(-1.0, 1.0,
                                (nh,nc))
                                .astype(theano.config.floatX))
        
        # biases
        self.bi = theano.shared(name='bi',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        
        self.bc = theano.shared(name='bc',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))

        self.bf = theano.shared(name='bf',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        
        self.bo = theano.shared(name='bo',
                               value=numpy.zeros(nh,
                               dtype=theano.config.floatX))

        self.by = theano.shared(name='by',
                               value=numpy.zeros(nc,
                               dtype=theano.config.floatX))        

        self.h0 = theano.shared(name='h0',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        
        self.c0 = theano.shared(name='c0',
                                value=numpy.zeros(nh,
                                dtype=theano.config.floatX))
        
        

        self.params = [self.wi, self.wf, self.wc, self.wo, self.wy, 
                       self.ui, self.uf, self.uc, self.uo,
                                                  self.vo, 
                       self.bi, self.bf, self.bc, self.bo, self.by, 
                       self.h0, self.c0]
        
        x = T.matrix('x')
        y = T.ivector('y')
        
        def recurrence(x_t, h_tm1, c_tm1):
            i_t = T.nnet.sigmoid(T.dot(x_t, self.wi)
                                + T.dot(h_tm1, self.ui) + self.bi)
            cc_t = T.tanh(T.dot(x_t, self.wc)
                                + T.dot(h_tm1, self.uc) + self.bc)
            f_t = T.nnet.sigmoid(T.dot(x_t, self.wf)
                                + T.dot(h_tm1, self.uf) + self.bf)
            c_t = i_t * cc_t + f_t * c_tm1
            o_t = T.nnet.sigmoid(T.dot(x_t, self.wo)
                                + T.dot(h_tm1, self.uo) + T.dot(c_t, self.vo) + self.bo)
            h_t = o_t * T.tanh(c_t)
            y_t = T.nnet.softmax(theano.dot(h_t, self.wy) + self.by)
            return [h_t, c_t, y_t]
        
        [h, c, yt], _ = theano.scan(fn=recurrence,
                                sequences=x,
                                outputs_info=[self.h0, self.c0, None],n_steps=x.shape[0])
        
        #self.L1 = abs(self.w.sum()) + abs(self.wx.sum()) + abs(self.wh.sum())
                                  
        #self.L2_sqr = (self.w ** 2).sum() + (self.wx ** 2).sum() + (self.wh ** 2).sum()
        
        # n_bits,1,nc
        p_y_given_x = yt[:, 0, :]
        
        y_pred = T.argmax(p_y_given_x, axis=1)
        
        err = T.mean(T.neq(y_pred, y))
        
        # cost and gradients and learning rate
        lr = T.scalar('lr')

       
        parity_nll = -T.mean(T.log(p_y_given_x)
                               [T.arange(x.shape[0]), y])
        
        parity_gradients = T.grad(parity_nll, self.params)
        
        parity_updates = OrderedDict((p, p - lr*g)
                                       for p, g in
                                       zip(self.params, parity_gradients))

        # theano functions to compile
        self.classify_compute_err = theano.function(inputs=[x, y], outputs=err, allow_input_downcast=True)
        self.parity_train = theano.function(inputs=[x, y, lr],
                                              outputs=parity_nll,
                                              updates=parity_updates,
                                              allow_input_downcast=True
                                              )



#TODO: build and train a MLP to learn parity function
def test_mlp_parity(learning_rate=0.01, L1_reg=0.00, L2_reg=0.0001, n_epochs=2000,
             batch_size=100, n_hidden=500, n_hiddenLayers=1,
             verbose=False, nbit=8 ):
    
    ## choose nbit = 500 and 5000 for n_hiddenLayers = 1, and 150 for 8 bit nhiddenlayers = 10 and 250 for 12 bit nhiddenlayer = 10

    # generate datasets
    train_set = gen_parity_pair(nbit, 1000)
    valid_set = gen_parity_pair(nbit, 500)
    test_set  = gen_parity_pair(nbit, 100)

    # Convert raw dataset to Theano shared variables.
    train_set_x, train_set_y = shared_dataset(train_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    test_set_x, test_set_y = shared_dataset(test_set)
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size


    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    rng = numpy.random.RandomState(1234)
    
    
    classifier = myMLP(rng=rng, input=x, n_in=nbit, n_hidden=n_hidden, n_out=2, n_hiddenLayers=n_hiddenLayers)

    # the cost we minimize during training is the negative log likelihood of
    # the model plus the regularization terms (L1 and L2); cost is expressed
    # here symbolically
    cost = (
        classifier.negative_log_likelihood(y)
        + L1_reg * classifier.L1
        + L2_reg * classifier.L2_sqr
    )

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    test_model = theano.function(
        inputs=[index],
        outputs=classifier.errors(y),
        givens={
            x: test_set_x[index * batch_size:(index + 1) * batch_size],
            y: test_set_y[index * batch_size:(index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        inputs=[index],
        outputs=classifier.errors(y),
        givens={
            x: valid_set_x[index * batch_size:(index + 1) * batch_size],
            y: valid_set_y[index * batch_size:(index + 1) * batch_size]
        }
    )

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = [T.grad(cost, param) for param in classifier.params]

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs

    # given two lists of the same length, A = [a1, a2, a3, a4] and
    # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
    # element is a pair formed from the two lists :
    #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
    updates = [
        (param, param - learning_rate * gparam)
        for param, gparam in zip(classifier.params, gparams)
    ]

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    return train_nn(train_model, validate_model, test_model,
        n_train_batches, n_valid_batches, n_test_batches, n_epochs, verbose)
    
#TODO: build and train a RNN to learn parity function
# def test_rnn_parity(learning_rate=0.75, L1_reg=0.00, L2_reg=0.0001, n_epochs=1000,
#              batch_size=1000, n_hidden=20, verbose=False, nbit=8):
    
#     # generate datasets
#     train_set = gen_parity_pair(nbit, 1000)
#     valid_set = gen_parity_pair(nbit, 500)
#     test_set  = gen_parity_pair(nbit, 100)

#     # Convert raw dataset to Theano shared variables.
#     train_set_x, train_set_y = shared_dataset(train_set)
#     valid_set_x, valid_set_y = shared_dataset(valid_set)
#     test_set_x, test_set_y = shared_dataset(test_set)

#     n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
#     n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
#     n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

#     ######################
#     # BUILD ACTUAL MODEL #
#     ######################
#     print('... building the model')

#     # allocate symbolic variables for the data
#     index = T.lscalar()  # index to a [mini]batch
#     x = T.matrix('x')  # the data is presented as rasterized images
#     y = T.ivector('y')  # the labels are presented as 1D vector of
#                         # [int] labels

#     rng = numpy.random.RandomState(1234)

#     # TODO: construct a neural network, either MLP or CNN.
#     classifier = RNN(input=x,
#                        n_in=nbit,
#                        n_hidden=n_hidden,
#                        n_out=2)

#     # the cost we minimize during training is the negative log likelihood of
#     # the model plus the regularization terms (L1 and L2); cost is expressed
#     # here symbolically
#     cost = (
#         classifier.negative_log_likelihood(y)
#         + L1_reg * classifier.L1
#         + L2_reg * classifier.L2_sqr
#     )

#     # compiling a Theano function that computes the mistakes that are made
#     # by the model on a minibatch
#     test_model = theano.function(
#         inputs=[index],
#         outputs=classifier.errors(y),
#         givens={
#             x: test_set_x[index * batch_size:(index + 1) * batch_size],
#             y: test_set_y[index * batch_size:(index + 1) * batch_size]
#         }
#     )

#     validate_model = theano.function(
#         inputs=[index],
#         outputs=classifier.errors(y),
#         givens={
#             x: valid_set_x[index * batch_size:(index + 1) * batch_size],
#             y: valid_set_y[index * batch_size:(index + 1) * batch_size]
#         }
#     )

#     # compute the gradient of cost with respect to theta (sotred in params)
#     # the resulting gradients will be stored in a list gparams
#     gparams = [T.grad(cost, param) for param in classifier.params]

#     # specify how to update the parameters of the model as a list of
#     # (variable, update expression) pairs

#     # given two lists of the same length, A = [a1, a2, a3, a4] and
#     # B = [b1, b2, b3, b4], zip generates a list C of same size, where each
#     # element is a pair formed from the two lists :
#     #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
#     updates = [
#         (param, param - learning_rate * gparam)
#         for param, gparam in zip(classifier.params, gparams)
#     ]

#     # compiling a Theano function `train_model` that returns the cost, but
#     # in the same time updates the parameter of the model based on the rules
#     # defined in `updates`
#     train_model = theano.function(
#         inputs=[index],
#         outputs=cost,
#         updates=updates,
#         givens={
#             x: train_set_x[index * batch_size: (index + 1) * batch_size],
#             y: train_set_y[index * batch_size: (index + 1) * batch_size]
#         }
#     )

#     ###############
#     # TRAIN MODEL #
#     ###############
#     print('... training')

#     return train_nn(train_model, validate_model, test_model,
#         n_train_batches, n_valid_batches, n_test_batches, n_epochs, verbose)

    
def test_rnn_parity(**kwargs):
    # process input arguments
    param = {
        'lr': 0.0970806646812754,
        'verbose': True,
        'decay': True,
        'nhidden': 10,
        'seed': 345,
        'nepochs': 60,
        'n_bit':8
        }
    '''
    param = {
        'lr': 1,
        'verbose': True,
        'decay': True,
        'nhidden': 10,
        'seed': 345,
        'nepochs': 60,
        'n_bit':12
        }
    '''
    param_diff = set(kwargs.keys()) - set(param.keys())
    if param_diff:
        raise KeyError("invalid arguments:" + str(tuple(param_diff)))
    param.update(kwargs)

    if param['verbose']:
        for k,v in param.items():
            print("%s: %s" % (k,v))
    
    # generate datasets
    train_set = gen_parity_pair(param['n_bit'], 1000)
    valid_set = gen_parity_pair(param['n_bit'], 500)
    test_set  = gen_parity_pair(param['n_bit'], 100)

 

    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set

    train_set_y = gen_parity_rnn(train_set_x)
    valid_set_y = gen_parity_rnn(valid_set_x)
    test_set_y = gen_parity_rnn(test_set_x)
    
    train_set_x = numpy.asarray(train_set_x)
    train_set_y = numpy.asarray(train_set_y).astype('int32')
    valid_set_x = numpy.asarray(valid_set_x)
    valid_set_y = numpy.asarray(valid_set_y).astype('int32')
    test_set_x = numpy.asarray(test_set_x)
    test_set_y = numpy.asarray(test_set_y).astype('int32')
    

    # instanciate the model
    numpy.random.seed(param['seed'])
    
    print('... building the model')
    
    nclasses = 2

    rnn = RNN(
        nh=param['nhidden'],
        nc=nclasses)

    # train with early stopping on validation set
    print('... training')
    best_valid_error = numpy.inf
    param['clr'] = param['lr']
    for e in range(param['nepochs']):

        param['ce'] = e
       
        for i, (x, y) in enumerate(zip(train_set_x, train_set_y)):
            rnn.parity_train(x.reshape(x.shape[0],1), y, param['clr'])
#           
            sys.stdout.flush()

        # evaluation // back into the real world : idx -> words
        test_error = [rnn.classify_compute_err(x.reshape(x.shape[0],1),y) for i, (x, y) in enumerate(zip(test_set_x, test_set_y))]
        test_error = numpy.mean(test_error)
        valid_error = [rnn.classify_compute_err(x.reshape(x.shape[0],1),y) for i, (x, y) in enumerate(zip(valid_set_x, valid_set_y))]
        valid_error = numpy.mean(valid_error)
        # evaluation // compute the accuracy using conlleval.pl

        if valid_error < best_valid_error:

#  
            best_valid_error = valid_error
            best_test_error = test_error
            if param['verbose']:
                print('NEW BEST: epoch', e,
                      'best valid error', best_valid_error*100,
                      'test error', best_test_error*100)

            param['be'] = e


        else:
            if param['verbose']:
                print('')

        # learning rate decay if no improvement in 10 epochs
        if param['decay'] and abs(param['be']-param['ce']) >= 10:
            param['clr'] *= 0.5
#             rnn = best_rnn

        if param['clr'] < 1e-5:
            break

        if param['verbose']:
            print('NEW BEST: epoch', e,
                  'best valid error', best_valid_error*100,
                  'test error', best_test_error*100)


#TODO: build and train a LSTM to learn parity function
def test_lstm_parity(**kwargs):
    
   
    # process input arguments
    param = {
        'lr': 2,
        'verbose': True,
        'nhidden': 1,
        'seed': 345,
        'nepochs': 100,
        'n_bit':8
        }

    '''
    param = {
        'lr': 3,
        'verbose': True,
        'nhidden': 1,
        'seed': 345,
        'nepochs': 100,
        'n_bit':12
        }
    '''

    param_diff = set(kwargs.keys()) - set(param.keys())
    if param_diff:
        raise KeyError("invalid arguments:" + str(tuple(param_diff)))
    param.update(kwargs)

    if param['verbose']:
        for k,v in param.items():
            print("%s: %s" % (k,v))
    
    # generate datasets
    train_set = gen_parity_pair(param['n_bit'], 1000)
    valid_set = gen_parity_pair(param['n_bit'], 500)
    test_set  = gen_parity_pair(param['n_bit'], 100)

    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    train_set_y = gen_parity_rnn(train_set_x)
    valid_set_y = gen_parity_rnn(valid_set_x)
    test_set_y = gen_parity_rnn(test_set_x)
    
    train_set_x = numpy.asarray(train_set_x)
    train_set_y = numpy.asarray(train_set_y).astype('int32')
    valid_set_x = numpy.asarray(valid_set_x)
    valid_set_y = numpy.asarray(valid_set_y).astype('int32')
    test_set_x = numpy.asarray(test_set_x)
    test_set_y = numpy.asarray(test_set_y).astype('int32')
    

    # instanciate the model
    numpy.random.seed(param['seed'])
    

    print('... building the model')
    
    nclasses = 2

    lstm = LSTM(
        nh=param['nhidden'],
        nc=nclasses)

    # train with early stopping on validation set
    print('... training')
    best_valid_error = numpy.inf
    param['clr'] = param['lr']
    for e in range(param['nepochs']):

        param['ce'] = e
  
        for i, (x, y) in enumerate(zip(train_set_x, train_set_y)):
            lstm.parity_train(x.reshape(x.shape[0],1), y, param['clr'])

            sys.stdout.flush()

        # evaluation // back into the real world : idx -> words
        test_error = [lstm.classify_compute_err(x.reshape(x.shape[0],1),y) for i, (x, y) in enumerate(zip(test_set_x, test_set_y))]
        test_error = numpy.mean(test_error)
        valid_error = [lstm.classify_compute_err(x.reshape(x.shape[0],1),y) for i, (x, y) in enumerate(zip(valid_set_x, valid_set_y))]
        valid_error = numpy.mean(valid_error)
        # evaluation // compute the accuracy using conlleval.pl

        if valid_error < best_valid_error:


            best_valid_error = valid_error
            best_test_error = test_error
            if param['verbose']:
                print('NEW BEST: epoch', e,
                      'best valid error', best_valid_error * 100,
                      'test error', best_test_error * 100)


            param['be'] = e


        else:
            if param['verbose']:
                print('')


        if param['verbose']:
            print('NEW BEST: epoch', e,
                  'best valid error', best_valid_error * 100,
                  'test error', best_test_error * 100)



    
if __name__ == '__main__':
    test_lstm_parity()
